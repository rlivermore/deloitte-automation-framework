# README

## What is the Deloitte Automation Framework?

The Deloitte Automation Framework is an automation framework template built around Selenium and Appium for automating the following apps:

* Web applications
* Android apps
* iOS apps (requires OS X)
* Windows classic and Metro applications (requires Windows 10)

## Who is this for?

Any Deloitte technologist who needs to roll out an automation solution for their client and have deemed Selenium/Appium to be suitable.

## How do I get started?

Feel free to create a fork of this repository.

## How do I contribute to this project?

Please observe good software development standards to create a new branch for your feature/change. Once complete, commit your changes and raise a pull request.

## Who do I collaborate with to improve this project?

See the commit log for active contributors and get in touch with one of those people, they'll be able to point you in the right direction.