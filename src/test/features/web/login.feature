@web
Feature: Login
  Users should be able to log into web app
  If the email or password is incorrect then user should see an error

Scenario Outline: Logging into web app
  Given I am a web user on the home page
  When I navigate to the sign in portal
  And I enter my email as <email>
  And I enter my password as <password>
  And I click Log In
  Then I should be on the landing page

  Examples:
  | email                     | password      |
  | rlivermore@deloitte.co.uk | ***           |