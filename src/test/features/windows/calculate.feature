@windows
Feature: Calculate
  Users should be able to perform simple calculations

Scenario Outline: Perform two integer calculations
  Given I am a calculator user
  When I press <num1> for my first number
  And I press <function> function
  And I press <num2> for my second number
  And I press equals
  Then I should be shown <result> as the result

  Examples:
  | num1 | function | num2 | result |
  |    1 | plus     |    9 |     10 |
  |    2 | plus     |    8 |     10 |
  |    3 | minus    |    7 |     -4 |
  |    8 | divide   |    4 |      2 |
  |    5 | multiply |    5 |     25 |
  |    6 | minus    |    3 |      3 |