@android
Feature: Login
  Users should be able to log into Android app
  If the email or password is incorrect then user should see an error

Scenario Outline: Logging into mobile app
  Given I am an android user
  When I tap SIGN IN
  And I enter <email> as my email
  And I enter <password> as my password
  And I tap SIGN IN again
  Then I should be on the app landing page

  Examples:
  | email                     | password |
  | rlivermore@deloitte.co.uk | ***      |