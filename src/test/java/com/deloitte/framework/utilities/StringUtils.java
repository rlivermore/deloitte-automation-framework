package com.deloitte.framework.utilities;

public class StringUtils {
    public static boolean IsNullOrEmpty(String input) {
        return input == null || "".equals(input);
    }
}
