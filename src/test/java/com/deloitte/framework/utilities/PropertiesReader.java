package com.deloitte.framework.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

  private static Properties properties = new Properties();

  public static void load(String path) {
    try {
      InputStream input = new FileInputStream(path);

      properties.load(input);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static Object get(String key) {
    return properties.get(key);
  }
}
