package com.deloitte.framework;

public enum AppType {
  ANDROID,
  CHROME,
  FIREFOX,
  HTMLUNIT,
  IE,
  IOS,
  OPERA,
  PHANTOMJS,
  SAFARI
}
