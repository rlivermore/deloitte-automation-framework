package com.deloitte.framework.driver;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public interface DriverSetup {
    RemoteWebDriver getWebDriver(DesiredCapabilities desiredCapabilities);
    DesiredCapabilities getDesiredCapabilities();
}
