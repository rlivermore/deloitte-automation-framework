package com.deloitte.framework.driver;

import com.deloitte.framework.utilities.PropertiesReader;
import io.appium.java_client.windows.WindowsDriver;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static com.deloitte.framework.driver.DriverType.FIREFOX;

public class DriverFactory {
  private WebDriver driver;
  private DriverType selectedDriver;

  private final DriverType defaultDriver = FIREFOX;

  private String browser = defaultDriver.toString().toUpperCase();
  private boolean useRemoteWebDriver = false;

  public WebDriver getWebDriver() {
    browser = ((String)PropertiesReader.get("browser")).toUpperCase();
    useRemoteWebDriver = Boolean.parseBoolean((String) PropertiesReader.get("remote"));

    if (null == driver) {
      selectedDriver = determineBrowser();
      DesiredCapabilities desiredCapabilities = selectedDriver.getDesiredCapabilities();
      establishDriver(desiredCapabilities);
    }

    driver.manage().timeouts().implicitlyWait(Long.parseLong((String) PropertiesReader.get("wait.implicit")), TimeUnit.valueOf((String) PropertiesReader.get("wait.timeunit")));

    return driver;
  }

  public RemoteWebDriver getWindowsDriver() {
    useRemoteWebDriver = Boolean.parseBoolean((String) PropertiesReader.get("remote"));

    selectedDriver = DriverType.WINDOWS;
    DesiredCapabilities desiredCapabilities = selectedDriver.getDesiredCapabilities();

    RemoteWebDriver winDriver = selectedDriver.getWebDriver(desiredCapabilities);
    winDriver.manage().timeouts().implicitlyWait(Long.parseLong((String) PropertiesReader.get("wait.implicit")), TimeUnit.valueOf((String) PropertiesReader.get("wait.timeunit")));

    return winDriver;
  }

  public RemoteWebDriver getAndroidDriver() {
    useRemoteWebDriver = Boolean.parseBoolean((String) PropertiesReader.get("remote"));

    selectedDriver = DriverType.ANDROID;
    DesiredCapabilities desiredCapabilities = selectedDriver.getDesiredCapabilities();

    RemoteWebDriver androidDriver = selectedDriver.getWebDriver(desiredCapabilities);
    androidDriver.manage().timeouts().implicitlyWait(Long.parseLong((String) PropertiesReader.get("wait.implicit")), TimeUnit.valueOf((String) PropertiesReader.get("wait.timeunit")));

    return androidDriver;
  }

  public void quitDriver() {
    if (null != driver) {
      driver.quit();
      driver = null;
    }
  }

  private DriverType determineBrowser() {
    DriverType driverType = defaultDriver;

    try {
      driverType = DriverType.valueOf(browser);
    } catch (IllegalArgumentException invalid) {
      System.err.println("Unknown driver specified. Defaulting to '" + driverType + "'.");
    }

    return driverType;
  }

  private void establishDriver(DesiredCapabilities desiredCapabilities) {
    if (useRemoteWebDriver) {
      URL seleniumGridURL = null;

      try {
        seleniumGridURL = new URL(System.getProperty("gridURL"));
      } catch (MalformedURLException e) {
        e.printStackTrace();
      }

      String desiredBrowserVersion = System.getProperty("gridBrowserVersion");
      String desiredPlatform = System.getProperty("gridPlatform");

      if (null != desiredPlatform && !desiredPlatform.isEmpty()) {
        desiredCapabilities.setPlatform(Platform.valueOf(desiredPlatform.toUpperCase()));
      }

      if (null != desiredBrowserVersion && !desiredBrowserVersion.isEmpty()) {
        desiredCapabilities.setVersion(desiredBrowserVersion);
      }

      driver = new RemoteWebDriver(seleniumGridURL, desiredCapabilities);
    } else {
      driver = selectedDriver.getWebDriver(desiredCapabilities);
    }
  }
}