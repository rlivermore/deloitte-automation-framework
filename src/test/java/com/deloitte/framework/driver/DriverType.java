package com.deloitte.framework.driver;

import com.deloitte.framework.utilities.PropertiesReader;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.windows.WindowsDriver;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import io.github.bonigarcia.wdm.PhantomJsDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum DriverType implements DriverSetup {
  FIREFOX {
    @Override
    public DesiredCapabilities getDesiredCapabilities() {
      return DesiredCapabilities.firefox();
    }

    @Override
    public RemoteWebDriver getWebDriver(DesiredCapabilities desiredCapabilities) {
      return new FirefoxDriver(desiredCapabilities);
    }
  },

  CHROME {
    @Override
    public DesiredCapabilities getDesiredCapabilities() {
      System.setProperty("webdriver.chrome.driver", (String) PropertiesReader.get("driver.location"));

      DesiredCapabilities capabilities = DesiredCapabilities.chrome();
      capabilities.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check", "--start-maximized"));
      HashMap<String, String> chromePreferences = new HashMap<String, String>();
      chromePreferences.put("profile.password_manager_enabled", "false");
      capabilities.setCapability("chrome.prefs", chromePreferences);
      return capabilities;
    }

    @Override
    public RemoteWebDriver getWebDriver(DesiredCapabilities desiredCapabilities) {
      ChromeOptions options = new ChromeOptions();
      options.merge(desiredCapabilities);

      return new ChromeDriver(options);
    }
  },

  PHANTOMJS {
    @Override
    public DesiredCapabilities getDesiredCapabilities() {
      DesiredCapabilities capabilities = DesiredCapabilities.phantomjs();
      final List<String> cliArguments = new ArrayList<String>();
      cliArguments.add("--web-security=false");
      cliArguments.add("--ssl-protocol=any");
      cliArguments.add("--ignore-ssl-errors=true");
      cliArguments.add("--webdriver-loglevel=NONE");
      capabilities.setCapability("phantomjs.cli.args", cliArguments);
      capabilities.setCapability("takesScreenshot", true);
      Logger.getLogger(PhantomJSDriverService.class.getName()).setLevel(Level.OFF);
      return capabilities;
    }

    @Override
    public RemoteWebDriver getWebDriver(DesiredCapabilities desiredCapabilities) {
      PhantomJsDriverManager.getInstance().setup();
      return new PhantomJSDriver(desiredCapabilities);
    }
  },

  IE {
    @Override
    public DesiredCapabilities getDesiredCapabilities() {
      DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
      capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
      capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
      capabilities.setCapability("requireWindowFocus", true);
      return capabilities;
    }

    @Override
    public RemoteWebDriver getWebDriver(DesiredCapabilities desiredCapabilities) {
      InternetExplorerDriverManager.getInstance().setup();
      return new InternetExplorerDriver(desiredCapabilities);
    }
  },

  EDGE {
    @Override
    public DesiredCapabilities getDesiredCapabilities() {
      return DesiredCapabilities.edge();
    }

    @Override
    public RemoteWebDriver getWebDriver(DesiredCapabilities desiredCapabilities) {
      return new EdgeDriver(desiredCapabilities);
    }
  },

  SAFARI {
    @Override
    public DesiredCapabilities getDesiredCapabilities() {
      DesiredCapabilities capabilities = DesiredCapabilities.safari();
      capabilities.setCapability("safari.cleanSession", true);
      return capabilities;
    }

    @Override
    public RemoteWebDriver getWebDriver(DesiredCapabilities desiredCapabilities) {
      return new SafariDriver(desiredCapabilities);
    }
  },

  WINDOWS {
    @Override
    public DesiredCapabilities getDesiredCapabilities() {
      DesiredCapabilities capabilities = new DesiredCapabilities();
      capabilities.setCapability("app", "C:\\WINDOWS\\system32\\calc.exe");
      capabilities.setCapability("platformName", "Windows");
      capabilities.setCapability("deviceName", "WindowsPC");
      return capabilities;
    }

    @Override
    public RemoteWebDriver getWebDriver(DesiredCapabilities desiredCapabilities) {
      try {
        return new WindowsDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
      } catch (MalformedURLException e) {
        e.printStackTrace();
        return null;
      }
    }
  },

  ANDROID {
    @Override
    public DesiredCapabilities getDesiredCapabilities() {
      DesiredCapabilities capabilities = new DesiredCapabilities();
      capabilities.setCapability("appPackage", "com.mobimate.cwttogo");
      capabilities.setCapability("browserName", "");
      capabilities.setCapability("platformName", "Android");
      capabilities.setCapability("platformVersion", "6.0");
      capabilities.setCapability("deviceName", "AndroidDevice");
      capabilities.setCapability("newCommandTimeout", "60");
      capabilities.setCapability("deviceReadyTimeout", "60");
      capabilities.setCapability("androidDeviceReadyTimeout", "60");
      capabilities.setCapability("avd", "AVD_for_Nexus_7_by_Google");
      capabilities.setCapability("appActivity", "com.worldmate.ui.activities.SplashActivity");
      return capabilities;
    }

    @Override
    public RemoteWebDriver getWebDriver(DesiredCapabilities desiredCapabilities) {
      try {
        return new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
      } catch (MalformedURLException e) {
        e.printStackTrace();
        return null;
      }
    }
  }
}
