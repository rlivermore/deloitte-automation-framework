package com.deloitte.testing.web.steps;

import com.deloitte.testing.web.pages.HomePage;
import com.deloitte.testing.web.pages.LandingPage;
import com.deloitte.testing.web.pages.LoginPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class LoginSteps extends BaseSteps {

  private HomePage homePage;
  private LoginPage loginPage;
  private LandingPage landingPage;

  @Given("^I am a web user on the home page$")
  public void I_am_a_web_user() {
    init();

    homePage = new HomePage(driver);
  }

  @When("^I navigate to the sign in portal$")
  public void I_navigate_to_the_sign_in_portal() {
    loginPage = homePage.navigateToSignInPage();
    loginPage.upgradeMessageCheck();
    loginPage.logInAlreadyRegistered();
  }

  @And("^I enter my email as (.*)")
  public void And_I_enter_my_email_as(String email) {
    loginPage.enterEmail(email);
  }

  @And("^I enter my password as (.*)$")
  public void And_I_enter_my_password_as(String password) {
    loginPage.enterPassword(password);
  }

  @And("^I click Log In$")
  public void And_I_click_log_in() {
    landingPage = loginPage.clickLogInButton();
  }

  @Then("^I should be on the landing page$")
  public void I_should_be_on_the_landing_page() {
    Assert.assertEquals(landingPage.getTitle(), "Home - UK-Deloitte UK - DELOITTE-EMEA - myCWT");

    quitDriver();
  }
}
