package com.deloitte.testing.web.pages;

import com.deloitte.framework.utilities.PropertiesReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {

  @FindBy(how = How.XPATH, xpath = "//a[@href='https://sso.carlsonwagonlit.com/login.do']")
  private WebElement portalSignInLink;

  public HomePage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);

    driver.get((String) PropertiesReader.get("web.url"));
  }

  public LoginPage navigateToSignInPage() {
    portalSignInLink.click();
    return new LoginPage(driver);
  }
}