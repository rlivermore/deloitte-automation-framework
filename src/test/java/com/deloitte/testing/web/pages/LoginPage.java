package com.deloitte.testing.web.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

  @FindBy(how = How.XPATH, xpath = "//a[text()='Log in']")
  private WebElement logInAlreadyRegistered;

  @FindBy(how = How.NAME, name = "pf.username")
  private WebElement emailField;
  @FindBy(how = How.NAME, name = "pf.pass")
  private WebElement passwordField;
  @FindBy(how = How.XPATH, xpath = "//button[text()='Log in']")
  private WebElement logInButton;

  public LoginPage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  public void logInAlreadyRegistered() {
    logInAlreadyRegistered.click();
  }

  public void enterEmail(String email) {
    emailField.sendKeys(email);
  }

  public void enterPassword(String password) {
    passwordField.sendKeys(password);
  }

  public LandingPage clickLogInButton() {
    logInButton.click();

    return new LandingPage(driver);
  }

  public void upgradeMessageCheck() {
    WebElement continueButton = driver.findElement(By.xpath("//a[text()='Continue To New Website']"));
    continueButton.click();
  }
}
