package com.deloitte.testing.web.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LandingPage extends BasePage {

  public LandingPage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }
}
