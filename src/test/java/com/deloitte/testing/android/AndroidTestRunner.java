package com.deloitte.testing.android;

import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.*;
import ru.stqa.selenium.factory.WebDriverPool;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

@CucumberOptions(
  features = { "src/test/features/android" }
)
public class AndroidTestRunner extends AbstractTestNGCucumberTests {

  @BeforeSuite
  public void beforeSuite() {
    System.out.println("*****Before Suite*****");
  }

  @BeforeClass
  public void beforeClass() {
    System.out.println("*****Before Class*****");
  }

  @BeforeMethod
  public void beforeMethod (ITestContext context) {
    System.out.println("*****Before Method*****");
  }

  @Before("@android")
  public void before() {
    System.out.println("*****Before*****");
  }

  @After("@android")
  public void after() {
    System.out.println("*****After*****");
  }

  @AfterMethod
  public void afterMethod(ITestContext context) {
    System.out.println("*****After Method*****");
  }

  @AfterClass
  public void afterClass() {
    System.out.println("*****After Class*****");
  }

  @AfterSuite
  public void afterSuite() {
    System.out.println("*****After Suite*****");
  }
}