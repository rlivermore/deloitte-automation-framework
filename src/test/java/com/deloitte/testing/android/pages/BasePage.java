package com.deloitte.testing.android.pages;

import org.openqa.selenium.WebDriver;

public class BasePage {

  protected WebDriver driver;

  public BasePage(WebDriver driver) {
    this.driver = driver;
  }

  public String getTitle() {
    return driver.getTitle();
  }
}
