package com.deloitte.testing.android.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

  @FindBy(id = "com.mobimate.cwttogo:id/on_boarding_sign_in_button")
  public WebElement signInButtonSplash;
  @FindBy(xpath = "//android.widget.EditText[@text='Email']")
  public WebElement emailField;
  @FindBy(xpath = "//android.widget.EditText[@text='Password']")
  public WebElement passwordField;
  @FindBy(id = "com.mobimate.cwttogo:id/find_user_btn")
  public WebElement nextButton;
  @FindBy(id = "com.mobimate.cwttogo:id/enter_password_btn")
  public WebElement signInButtonPassword;

  public LoginPage(WebDriver webDriver) {
    super(webDriver);

    PageFactory.initElements(driver, this);
  }

  public void clickSignIn() {
    signInButtonSplash.click();
  }

  public void enterEmail(String email) {
    emailField.sendKeys(email);
    nextButton.click();
  }

  public void enterPassword(String password) {
    passwordField.sendKeys(password);
    signInButtonPassword.click();
  }
}