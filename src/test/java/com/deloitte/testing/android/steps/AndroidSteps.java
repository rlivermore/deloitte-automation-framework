package com.deloitte.testing.android.steps;

import com.deloitte.testing.android.pages.LoginPage;
import cucumber.api.java.en.*;

public class AndroidSteps extends BaseSteps {
  private LoginPage loginPage;

  @Given("^I am an android user$")
  public void I_am_a_mobile_user() {
    init();

    loginPage = new LoginPage(driver);
  }

  @When("^I tap SIGN IN$")
  public void When_I_tap_sign_in() {
    loginPage.clickSignIn();
  }

  @And("^I enter (.*) as my email$")
  public void And_I_enter_my_email(String email) {
    loginPage.enterEmail(email);
  }

  @And("^I enter (.*) as my password$")
  public void And_I_enter_my_password(String password) {
    loginPage.enterPassword(password);
  }

  @And("^I tap SIGN IN again$")
  public void And_I_tap_sign_in_again() {
    loginPage.clickSignIn();
  }

  @Then("^I should be on the app landing page$")
  public void I_should_be_on_the_app_landing_page() {
    // TODO - Assert I'm on the landing page

    quitDriver();
  }
}
