package com.deloitte.testing.android.steps;

import com.deloitte.framework.driver.DriverFactory;
import com.deloitte.framework.utilities.PropertiesReader;
import org.openqa.selenium.WebDriver;

public class BaseSteps {
  protected WebDriver driver;

  protected void init() {
    PropertiesReader.load("src/test/resources/testing.properties");

    driver = new DriverFactory().getAndroidDriver();
  }

  protected void quitDriver() {
    driver.quit();
    driver = null;
  }
}
