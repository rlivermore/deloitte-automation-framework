package com.deloitte.testing.windows.steps;

import com.deloitte.testing.windows.pages.CalculatorPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CalculateSteps extends BaseSteps {

  private CalculatorPage calculatorPage;

  @Given("^I am a calculator user$")
  public void I_am_a_web_user() {
    init();

    calculatorPage = new CalculatorPage(driver);
  }

  @When("^I press (\\d+) for my first number$")
  public void iPressNumForMyFirstNumber(int arg0) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
  }

  @And("^I press (.*) function$")
  public void iPressFunctionFunction(String arg0) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
  }

  @And("^I press (\\d+) for my second number$")
  public void iPressNumForMySecondNumber(int arg0) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
  }

  @And("^I press equals$")
  public void iPressEquals() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
  }

  @Then("^I should be shown (.*) as the result$")
  public void iShouldBeShownResultAsTheResult(String arg0) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
  }
}
