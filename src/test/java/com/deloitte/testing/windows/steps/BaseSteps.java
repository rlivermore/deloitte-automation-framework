package com.deloitte.testing.windows.steps;

import com.deloitte.framework.driver.DriverFactory;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BaseSteps {
  protected RemoteWebDriver driver;

  protected void init() {
    driver = new DriverFactory().getWindowsDriver();
  }

  protected void quitDriver() {
    driver.quit();
    driver = null;
  }
}
