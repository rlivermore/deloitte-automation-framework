package com.deloitte.testing.windows.pages;

import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class BasePage {

  protected RemoteWebDriver driver;

  public BasePage(RemoteWebDriver driver) {
    this.driver = driver;
  }

  public String getTitle() {
    return driver.getTitle();
  }
}
