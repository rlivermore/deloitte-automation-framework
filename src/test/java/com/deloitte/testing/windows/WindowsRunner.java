package com.deloitte.testing.windows;

import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.ITestContext;
import org.testng.annotations.*;

@CucumberOptions(
        features = { "src/test/features/windows" }
)
public class WindowsRunner extends AbstractTestNGCucumberTests {

  @BeforeSuite
  public void beforeSuite() {
    System.out.println("*****Before Suite*****");
  }

  @BeforeClass
  public void beforeClass() {
    System.out.println("*****Before Class*****");
  }

  @BeforeMethod
  public void beforeMethod (ITestContext context) {
    System.out.println("*****Before Method*****");
  }

  @Before("@windows")
  public void before() {
    System.out.println("*****Before*****");
  }

  @After("@windows")
  public void after() {
    System.out.println("*****After*****");
  }

  @AfterMethod
  public void afterMethod(ITestContext context) {
    System.out.println("*****After Method*****");
  }

  @AfterClass
  public void afterClass() {
    System.out.println("*****After Class*****");
  }

  @AfterSuite
  public void afterSuite() {
    System.out.println("*****After Suite*****");
  }
}
